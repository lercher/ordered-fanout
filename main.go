package main

import (
	"context"
	"log"
	"math/rand"
	"sync"
	"sync/atomic"
	"time"
)

var rng = rand.New(rand.NewSource(0))

func main() {
	log.Println("This is ordered fanout sample, (C) 2020, 2023 by Martin Lercher")

	// config:
	count := 4
	workitems := 50 * count

	var msOfHypotheticalWork int64
	var busy int64

	ready := make(chan Ready[int], 1)
	mu := new(sync.Mutex)
	mu.Lock()

	var t1 time.Duration
	go func() {
		t0 := time.Now()
		for r := range ready {
			_ = r
			log.Println("consumed-result:", r.Result, "err:", r.Err, "after wall-clock time:", time.Now().Sub(t0))
		}
		t1 = time.Now().Sub(t0)
		log.Println("Ready DONE")
		mu.Unlock()
	}()

	fan := New(count, ready)
	for i := 1; i <= workitems; i++ {
		log.Println("Do", i)
		i := i // closure!
		fan.Do(context.Background(), func(ctx context.Context) (int, error) {
			n := rng.Intn(200) + 0
			atomic.AddInt64(&msOfHypotheticalWork, int64(n))
			dur := time.Duration(n) * time.Millisecond			
			log.Println(">", i, "works for", dur, "...")
			done := time.After(dur)
			for {
				select {
				default:
					atomic.AddInt64(&busy, 1) // busiliy spinning the CPU
				case <-done:
					log.Println("<", i, "done")
					return i, nil
				}
			}
		})
	}

	log.Println("Queuing DONE")
	fan.Close()
	log.Println("Fan DONE")
	mu.Lock()
	log.Println()

	scheduledWork := time.Duration(msOfHypotheticalWork) * time.Millisecond
	scheduledWorkPerGoRoutine := scheduledWork / time.Duration(count)
	log.Println("workitems count:      ", workitems)
	log.Println("go routines count:    ", count)
	log.Println("work scheduled total: ", scheduledWork)
	log.Println("work scheduled/count: ", scheduledWorkPerGoRoutine)
	log.Println("real-time:            ", t1)
	log.Println("ordering overhead:    ", t1-scheduledWorkPerGoRoutine)
	log.Println("busily counted to:    ", busy)
}
