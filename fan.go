package main

import (
	"context"
	"sync"
	"sync/atomic"
)

// Worker is the basic unit of work to be faned out and then ordered
type Worker[TResult any] func(ctx context.Context) (TResult, error)

type seq uint64

// Ready is the result of a unit of work
type Ready[TResult any] struct {
	Result TResult
	Err    error
}

// workItemWaiting is an internal structure to augment a Worker with a context and a sequence number
type workItemWaiting[TResult any] struct {
	work Worker[TResult]
	seq  seq
	ctx  context.Context
}

// workItemDone is an internal structure to capture the results of a workitem with a sequence number
type workItemDone[TResult any] struct {
	Ready[TResult]
	seq seq
}

// Fan enqueues units of work to N worker go-routines and sends results in order
// back to a given channel
type Fan[TResult any] struct {
	Ready       chan<- Ready[TResult]
	N           int
	workWaiting chan workItemWaiting[TResult]
	workDone    chan workItemDone[TResult]
	outOfSeq    map[seq]Ready[TResult]
	seqIn       uint64
	seqOut      seq
	wg          sync.WaitGroup
	out         sync.WaitGroup
}

// New creates and starts a new Fan with workerCount go routines.
// See Do for details.
func New[TResult any](workerCount int, ready chan<- Ready[TResult]) *Fan[TResult] {
	f := &Fan[TResult]{
		Ready:       ready,
		N:           workerCount,
		workWaiting: make(chan workItemWaiting[TResult], 1),
		workDone:    make(chan workItemDone[TResult], 1),
		outOfSeq:    make(map[seq]Ready[TResult]),
		seqIn:       0,
		seqOut:      1,
	}

	f.out.Add(1)
	go f.aggregator()

	f.wg.Add(workerCount)
	for i := 0; i < workerCount; i++ {
		go f.worker()
	}

	return f
}

func (f *Fan[TResult]) worker() {
	defer f.wg.Done()

	for wiw := range f.workWaiting {
		select {
		default:
			// normal action is do the work of the worker
			wi := Ready[TResult]{}
			wi.Result, wi.Err = wiw.work(wiw.ctx)
			f.workDone <- workItemDone[TResult]{
				Ready: wi,
				seq:   wiw.seq,
			}
		case <-wiw.ctx.Done():
			// context was canceled before we came to do work
			f.workDone <- workItemDone[TResult]{
				Ready: Ready[TResult]{
					Err: wiw.ctx.Err(),
				},
				seq: wiw.seq,
			}
		}
	}
}

func (f *Fan[TResult]) aggregator() {
	defer f.out.Done()

	for wid := range f.workDone {
		if wid.seq != f.seqOut {
			// need to store it for later, b/c it's not the next result in seq order
			f.outOfSeq[wid.seq] = wid.Ready
			// next seq in f.aggregator:
			// log.Println("keep:", wi.seq, "len:", len(f.aggregator))
			continue
		}
		
		// nothing to store, send the results to the client's Ready channel
		f.Ready <- wid.Ready

		// now lookup previously stored seq numbers
		for {
			f.seqOut++
			if other, ok := f.outOfSeq[f.seqOut]; ok {				
				f.Ready <- other // we have the next item in the aggregator, send it				
				delete(f.outOfSeq, f.seqOut) // now we're done with that item, delete it
			} else {
				// f.seqOut is not ready yet				
				break // wait for next item from f.workDone
			}
		}
	}
	close(f.Ready)
}

// Close closes all channels involved and waits for completion of all
// of a Fan's go routines
func (f *Fan[TResult]) Close() {
	close(f.workWaiting)
	f.wg.Wait() // all workers done
	close(f.workDone)
	f.out.Wait() // aggregator done
}

// Do adds a unit of work to be faned-out.
// A unit of work is a function taking a context and returning (TResult, error).
//
// Do blocks until one of the N worker go routines picks it up.
// Units of work are executed in parallel, however,
// their results are sent to Ready in order of submission
// to Do() in terms of an atomically increased 64bit sequence number.
//
// If later units of work complete early, they are parked for future
// publishing to Ready. Canceling a unit of work's context after parking
// is ignored. Parked units are sent to ready after their previous unit
// was sent to Ready. If units of work won't terminate, parked units
// pile up until there are no more ressources.
//
// The two channels for enqueuing a work item and for passing
// the result to the fan-in ordering aggregator have a length of one,
// so a slow end consumer blocks a producer calling Do eventually.
// E.g. a totally non responsive consumer blocks Do() N+1 calls after
// the first two units of work are done.
func (f *Fan[TResult]) Do(ctx context.Context, w Worker[TResult]) {
	nextSeq := atomic.AddUint64(&f.seqIn, 1)
	wiw := workItemWaiting[TResult]{
		work: w,
		ctx:  ctx,
		seq:  seq(nextSeq),
	}
	f.workWaiting <- wiw
}
