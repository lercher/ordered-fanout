# Ordered Fan-Out

It's a sample program to show how to fan-out
some units of work over N go routines and to collect the
results in the order of scheduling these units of work.

I've recently re-worked the internals, and also changed the
unit of work type from an interface to a function with
a generic result type.

However, I've got too few use-cases to judge over this
latest change being useful or bad.
